import {Outlet} from "react-router-dom";
import _footer from "./_partials/_footer.jsx"

export default function Root() {

  return (
    <>
      <Outlet />
      <_footer id='page-footer' classes={'py-2'} />
    </>
  )
}