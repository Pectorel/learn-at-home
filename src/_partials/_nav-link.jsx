import PropTypes from "prop-types";
import "../style/_partial/_nav-link.css";

NavLink.propTypes = {
    href: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    classes: PropTypes.string,
};

export default function NavLink(props) {

    return (
        <a href={props.href} className={"nav-link mr-6" + (props.classes ? " " + props.classes : "")}>
            <i className={"fa-icon mr-2 " + props.icon}></i>
            <span>{props.title}</span>
        </a>
    )
}