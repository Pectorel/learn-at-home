import PropTypes from "prop-types";
import "../style/_partial/_header.css";
import NavLink from "./_nav-link.jsx";

Header.propTypes = {
    id: PropTypes.string,
    classes: PropTypes.string
};

export default function Header(props) {

    return (
        <header id={props.id} className={props.classes}>
            <div className="wrapper d-flex">
                <a href="/learn-at-home/app/dashboard" className={'mr-7'}>
                    <img src="/logo-text.jpg" alt="Learn at Home logo"/>
                </a>
                <nav className="d-flex">
                    <NavLink classes={'link'} href={'/learn-at-home/app/dashboard'} icon={'fa-solid fa-table-columns'} title={'Dashboard'}/>
                    <NavLink classes={'link'} href={'/learn-at-home/app/chat'} icon={'fa-solid fa-comments'} title={'Chat'}/>
                    <NavLink classes={'link'} href={'/learn-at-home/app/calendar'} icon={'fa-solid fa-calendar-days'} title={'Calendrier'}/>
                    <NavLink classes={'link'} href={'/learn-at-home/app/task-manager'} icon={'fa-solid fa-list-check'} title={'Tâches'}/>
                </nav>

            </div>

        </header>
    )
}