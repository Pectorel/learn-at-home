import PropTypes from "prop-types";
import "../style/_partial/_footer.css"


Footer.propTypes = {
  id: PropTypes.string,
  classes: PropTypes.string
};

export default function Footer(props) {

  return (
    <footer id={props.id} className={props.classes}>
      <div className="wrapper">
        <span>&copy; Learn@Home {new Date().getFullYear()}</span>
        <a href="mentions-legales">Mentions légales</a>
      </div>
    </footer>
  )
}