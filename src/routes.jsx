import {
  createBrowserRouter,
} from "react-router-dom";
import PageError from "./page-error.jsx";
import Root from "./root.jsx";
import App from "./App.jsx";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    errorElement: <PageError />,
  },
  {
    path: "/app",
    element: <App />,
    errorElement: <PageError />,
    children : [
      {
        path: "dashboard",
        element: <div>Dashboard</div>
      },
      {
        path: "chat",
        element: <div>Chat</div>
      },
      {
        path: "calendar",
        element: <div>Calendar</div>
      },
      {
        path: "task-manager",
        element: <div>Task Manager</div>
      },
    ],
  }

], {
  basename: "/learn-at-home/"
});

export default router