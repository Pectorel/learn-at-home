import "./style/page-error.css"

export default function PageError() {

  return (
    <>
      <section id="page-error" className="wrapper">
        <h1>404</h1>
        <p>La page que vous demandez n&apos;existe pas</p>
        <a href="/learn-at-home/">Retourner à l&apos;accueil</a>
      </section>
    </>
  )
}