import React from 'react'
import ReactDOM from 'react-dom/client'
import {
  RouterProvider,
} from 'react-router-dom';
import router from "./routes.jsx"
import "./style/libraries/reset.css"
import './style/main.css'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider basename="/learn-at-home/" router={router} />
  </React.StrictMode>,
)
