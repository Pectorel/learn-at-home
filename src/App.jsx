import {Outlet} from "react-router-dom";
import Header from "./_partials/_header.jsx";
import Footer from "./_partials/_footer.jsx";

function App() {

  return (
    <>
        <Header id='page-header' classes={'py-6'} />
        <Outlet />
        <Footer id='page-footer' classes={'py-2'} />

    </>
  )
}

export default App
