import { createLogger, defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// Custom logger to prevent postcss-calc Lexical error
const logger = createLogger();
const originalWarning = logger.warn;
logger.warn = (msg, options) => {
  if (msg.includes('vite:css') && msg.includes('Lexical error on line 1: Unrecognized text.')) return;
  originalWarning(msg, options);
};

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  base: '/learn-at-home/',
  customLogger: logger,
})
