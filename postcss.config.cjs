// eslint-disable-next-line no-undef
module.exports = {
  plugins: {
    '@csstools/postcss-global-data': {
      files: [
          "./src/style/components/_variables.css"
      ]
    },
    'postcss-import': {},
    'postcss-for': {},
    'postcss-simple-include': {},
    'postcss-preset-env': {
      stage: 2,
      features: {
        'custom-properties': {
          preserve: false
        }
      }
    },
    'postcss-dark-and-light': {},
    'postcss-calc': {},
  }
};